Sliding Windows by Blocks for Online Wavelet Discrete Transform Implementation.

In this paper we propose an online Wavelet Discrete Transform implementation scheme. Our proposal improves the execution time compared to the traditional sliding window method. Also, we modify the definition of the data window concept given in the original scheme. The experiments we performed show that the runtime cost of the proposed algorithm is better than that of the traditional sliding window method.

Wavelet; online; sliding window; dynamic programming.