Software piracy: model proposed

It is estimated that 61% of software is pirated in Chile, causing estimated losses of $ 382 million in 2011 (Business Software Alliance, 2012). The aim of this research is to identify the most important reasons behind the software's purchase and copy. A survey of 165 valid responses was used to investigate these reasons. In addition, this study offers a theoretical model that could explain the piracy behavior, which was developed from previous research into software piracy and the theory of planned behavior. This is relevant at country level because knowing the factors that motivate the piracy behavior will permit to define specific plans to eradicate it. The results reveal that the most important reasons to software purchase are: use the software all the time, required for the university or company and in case of problems one can use the technical support. Moreover, the most important reason to software piracy is because it is too expensive. Therefore, strategies to reduce software piracy should consider these reasons.

Piracy; Software; Postgraduate students; Explicative Factors; Chile.

