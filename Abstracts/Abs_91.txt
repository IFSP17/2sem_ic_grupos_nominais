A Domain-Transformation Approach to
Synthesize Read-Polarity-Once Boolean Functions


Efficient exact factoring algorithms are limited to read-once (RO) functions, where each variable appears exactly once at the final Boolean expression. However, these algorithms present two important constraints: (1) they do not consider incompletely specified Boolean functions (ISFs), and (2) they are not suitable for binate functions. To overcome the first drawback, an algorithm that finds RO expressions for ISF, whenever possible, is proposed. In respect to the second limitation, we propose a domain transformation that splits existing binate variables into two independent unate variables. Such a domain transformation leads to ISFs, which can be efficiently factored by applying the proposed algorithm. The combination of both contributions gives optimal results for a recently proposed broader class of Boolean functions called read-polarity-once (RPO) functions, where each polarity (positive and negative) of a variable appears at most once in the factored form. Experimental results carried out over ISCAS�85 benchmark circuits have shown that RPO functions are significantly more frequent than RO functions. 

Boolean functions, factoring, logic synthesis, read-once, read-polarity-once, digital circuits.
