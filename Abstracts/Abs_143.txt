NOTES ON "DEWEYIDS - THE KEY TO FINE-GRAINED MANAGEMENT OF XML DOCUMENTS''

Referring to the implementation and optimization of XTC as our prototype XDBMS, we discuss the experiences we have made with the application of DeweyIDs  since the publication our original paper. For this reason, we highlight the areas where DeweyIDs are the source of fine-grained management, processing flexibility, and  performance drivers in XDBMSs. The detailed results and progress accomplished can be found in the referenced literature.

