A Heuristic Approach for Blind Source Separation of Instant Mixtures.

In this paper we present a methodology for blind source separation (BSS) based on a coherence function to solve the problem of linear instantaneous mixtures of signals. The proposed methodology consists of minimizing the coherence function using a heuristic algorithm based on the simulating annealing method. Also, we derived an analytical expression of the coherence for the BSS model, in which it is found that independent and identically distributed (iid) Gaussian components can be recovered. Our results show satisfactory performance in comparison with traditional methods.

Blind source separation; second-order statistics; source extraction; Gaussian sources; simulated annealing.